import requests
from django.conf import settings


def lsi_fetch_token(app_id: str, api_key: str):
    response = requests.post(
        settings.LETSIGNIT_API_URL + "/auth/login",
        headers={
            "Content-Type": "application/json",
        },
        json={
            "app_id": app_id,
            "api_key": api_key,
        },
    )

    return response.json()["access_token"]


def lsi_start_session(token: str):
    response = requests.post(
        settings.LETSIGNIT_API_URL + "/onpremise/synchronizations",
        headers={
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
    )

    return response.json()["id"]


def lsi_push_data(token: str, session_id: str, scope: str, data: list):
    page = 1

    for entity in data:
        body = {
            "page": page,
            "per_page": 1,
            "total_" + scope: len(data),
            "total_page": len(data),
            scope: (entity,),
        }

        response = requests.post(
            settings.LETSIGNIT_API_URL
            + "/onpremise/synchronizations/"
            + session_id
            + "/"
            + scope,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token,
            },
            json=body,
        )

        page += 1

    return True
